#ifndef ARRAY_H
	#include "../includes/array.h"
#endif

#include <stdlib.h>


void initArray(Array *a, uint8_t initialSize) // array initialization
{
	a->array = (uint8_t *) malloc(initialSize * sizeof(uint8_t));
	a->used = 0;
	a->size = initialSize;
}

void insertElement(Array *a, uint8_t element) //inserts one element at the end of the array
{
	// a->used is the number of used indexes and a->used can go up to a->size 
	if (a->used == a->size) {
		a->size += 1;
		a->array = (uint8_t *)realloc(a->array, a->size * sizeof(uint8_t));
	}
	a->array[a->used++] = element;
}

void removeElement(Array *a, uint8_t size, uint8_t index) //removes element with the corresponding index from the array
{
	uint8_t i;
	uint8_t *temp = (uint8_t *) malloc((size - 1) * sizeof(uint8_t*));
	for (i = 0; i < a->size - 1 ; ++i)
		temp[i] = a->array[i];
	free(a->array);
	a->array = temp;
	a->size--;
	a->used--;
}

void freeArray(Array *a) //free allocated array memory
{
	free(a->array);
	a->array = NULL;
	a->used = a->size = 0;
}

void shiftArray(Array *a, uint8_t index) //array shifting starting with #index, will be used for path optimization
{
	uint8_t i;
	for (i = index; i < a->size - 2; ++i)
	{
		a->array[i] = a->array[i + 2];
	}
	removeElement(a, a->size, a->size);
	removeElement(a, a->size, a->size);
}

void processArray(Array *a) // path optimization algorithm
{
	uint8_t i;
	uint8_t turn_around_found = 1;
	while (turn_around_found == 1)
	{
		turn_around_found = 0;
		i = 1;
		while (i < a->size)
		{
			if (a->array[i] == 'B') //search for the B's and also check its "neighbours"
			{
				if ((a->array[i - 1] == 'L') && (a->array[i + 1] == 'R'))
					a->array[i - 1] = 'B';
				else if ((a->array[i - 1] == 'R') && (a->array[i + 1] == 'L'))
					a->array[i - 1] = 'B';
				else if ((a->array[i - 1] == 'S') && (a->array[i + 1] == 'S'))
					a->array[i - 1] = 'B';
				else if ((a->array[i - 1] == 'L') && (a->array[i + 1] == 'L'))
					a->array[i - 1] = 'S';
				else if ((a->array[i - 1] == 'S') && (a->array[i + 1] == 'L'))
					a->array[i - 1] = 'R';
				else if ((a->array[i - 1] == 'L') && (a->array[i + 1] == 'S'))
					a->array[i - 1] = 'R';

				shiftArray(a, i);
				turn_around_found = 1;
			}
			++i;
		}
	}
}
