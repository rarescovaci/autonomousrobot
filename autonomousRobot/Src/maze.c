
#include "../includes/global_variables.h"
#include "../includes/maze.h"
#include "../includes/line_follow.h"
#include "../includes/crossroads.h"
#include "../includes/motor_control.h"
#include "../includes/defines.h"

void checkForCrossroad()
{
	crossroad = 0; //crossroad code initialization
	
	if (((SENSOR_3 > THRESHOLD) || (SENSOR_4 > THRESHOLD) || (SENSOR_5 > THRESHOLD) || (SENSOR_6 > THRESHOLD)) && ((SENSOR_1 > THRESHOLD_R) || (SENSOR_8 > THRESHOLD)))
	{	// if robot is following line (sensors 4 or 5 detects black) and one outer sensor also detects black 
		turnaround_counter = 0;
				
		ref_l = DEFAULT_SPEED_REF; ref_r = DEFAULT_SPEED_REF;
		
		while ((SENSOR_1 > THRESHOLD_R) || (SENSOR_8 > THRESHOLD))
		{	// keep going straight until both outer sensors detect white, or until timeout for maze solved is reached
			if	(SENSOR_8 > THRESHOLD)
				crossroad |= 1u << 2; // 1xx means a left turn is possible
		
			if	(SENSOR_1 > THRESHOLD_R) // the crossroad code uses the least significant 3 bits of the crossroad variable
				crossroad |= 1u; // xx1 means a right turn is possible
			
			if (speed_control_enable_flag == 1)
			{
				controlSpeed();
				speed_control_enable_flag = 0;
				
				sensor_code = getSensorCode(); //read sensors
			
				if (sensor_code == 255) // if all sensors detect black at least TIMEOUT*10 ms, the robot found the maze exit
					++maze_solved_counter;
				else maze_solved_counter = 0;
			
				if (maze_solved_counter >= TIMEOUT)
				{
					maze_solved = 1;
					return;
				}
			}
		}
		
		maze_solved_counter = 0;	
		if (SENSOR_4 > THRESHOLD || SENSOR_5 > THRESHOLD) 
			crossroad |= 1u << 1; // check if going straight is possible in the crossroad, code x1x means going straigth is possible
	}
}

void manageCrossroad()
{
	if (mode == EXPLORATION_MODE) // in EXPLORATION MODE do
	{
		switch(crossroad) //decide what to do based on the crossroad code
		{
			case 1:
				turnRight(); //right turn 001
				break;
			case 3: //straight or right 011
				insertElement(&directions, 'S');
				goStraight();
				break;
			case 4: //left turn 100
				turnLeft();
				break;
			case 5:	// T crossroad: turn left 101
				insertElement(&directions, 'L');
				turnLeft();
				break;
			case 6: //left or forward: turn left with leap 110
				insertElement(&directions, 'L');
				turnLeft2();
				break;
			case 7: // X crossroad 111
				insertElement(&directions, 'L');
				turnLeft2();
				break;
		}
	}
	
	else // in OPTIMIZED MODE do
	{
		if (directions.array[0] == 'B') // if first decision is a turnaround, ignore it and go to the next element,
																		// the robot will turn around using the line follow algorithm
			crossroad_number = 1;
		if ((crossroad == 3) || (crossroad == 5) || (crossroad == 6) || (crossroad == 7)) // if it's a real crossroad
		{
			++crossroad_number; 
			if (crossroad_number > directions.size) //if crossroad number overflows in optimization mode, stop 
			{
				ref_l = 0; ref_r = 0;
				controlSpeed();
				while(1){}
			}
			else
			{
				switch(directions.array[crossroad_number - 1]) //choose the corresponding direction from the array
				{
					case 'S': goStraight();
						break;
					case 'L': turnLeft2(); //turn left with leap
						break;
					case 'R': turnRight2(); //turn right with leap
						break;
				}
			}
		}
		else
		{
			if (crossroad == 1) //if it is a right turn, turn right
				turnRight();
			else if (crossroad == 4) //if left turn, turn left
				turnLeft();
		}
	}
}

void solveMaze()
{	
	checkForCrossroad();
	if (maze_solved == 1) // if the end of the maze was found, return to main while loop
			return;
	else if (crossroad != 0) //if a crossroad or a turn is detected and maze is not solved, manage crossroad
			manageCrossroad();
	
	sensor_code = getSensorCode(); // read line sensors
	
	if (sensor_code == 0) // if line is lost, wait for time = TURNAROUND_ENABLE * 100 ms
	{
		if (turnaround_counter == TURNAROUND_ENABLE) //if counter reaches TURNAROUND ENABLE value, do a 180 degrees turn
		{
			turnAround();
			turnaround_counter = 0;
		}
		else ++turnaround_counter;
	}
	
	else   // if the line is found 
	{
			turnaround_counter = 0; // reset turnaround counter 
			if (prev_left_motor_direction == BACKWARD) 
			{
				ref_l = 0; ref_r = 0; // stop motors, wait for 100 ms and set direction to forward 
				controlSpeed();
				HAL_Delay(100);
				setDirection(FORWARD, FORWARD);
				prev_left_motor_direction = FORWARD;
			}
			followLine();
	}
}
