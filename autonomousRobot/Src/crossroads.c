
#include "../includes/global_variables.h"
#include "../includes/crossroads.h"
#include "../includes/motor_control.h"
#include "../includes/defines.h"

#define LEAP_TIME 100 //time (*10 ms) used to leap over lines in crossroads
#define LEAP_TIME_STRAIGHT 80

void turnLeft()
{
	  ref_l = 0; ref_r = DEFAULT_SPEED_REF;
		while ((SENSOR_3 < THRESHOLD) && (SENSOR_4 < THRESHOLD) && (SENSOR_5 < THRESHOLD))
		//while sensors 3 or 4 or 5 do not find the line
		{
			if (speed_control_enable_flag == 1)
			{	
				controlSpeed();
				speed_control_enable_flag = 0;
			}
		}
		orientation = (orientation + 90) % 360; // update robot's orientation
}

void turnLeft2() // used for turning left and leap over other lines
{		
	  ref_l = 0; ref_r = DEFAULT_SPEED_REF;
		uint8_t leap_counter = 0;
		while ((leap_counter < LEAP_TIME) || ((SENSOR_3 < THRESHOLD) && (SENSOR_4 < THRESHOLD)  && (SENSOR_5 < THRESHOLD)))
		//turn left for at least (LEAP_TIME * 10 ms) and until sensor 3, 4 or 5 finds the line
		{
			if (speed_control_enable_flag == 1)
			{	
				controlSpeed();
				speed_control_enable_flag = 0;
				++leap_counter;
			}
		}
		orientation = (orientation + 90) % 360;
}

void turnRight()
{		
	  ref_l = DEFAULT_SPEED_REF; ref_r = 0; 
		while ((SENSOR_4 < THRESHOLD) && (SENSOR_5 < THRESHOLD) && (SENSOR_6 < THRESHOLD))
		//while sensors 4 or 5 or 6 do not find the line
		{
			if (speed_control_enable_flag == 1)
			{	
				controlSpeed();
				speed_control_enable_flag = 0;
			}
		}
		orientation = (orientation + 270) % 360;
}

void turnRight2()// used for turning right and leap over other lines
{		
	  ref_l = DEFAULT_SPEED_REF; ref_r = 0; 
		uint8_t leap_counter = 0;
		while ((leap_counter < LEAP_TIME) || ((SENSOR_4 < THRESHOLD) && (SENSOR_5 < THRESHOLD) && (SENSOR_6 < THRESHOLD)))
		//turn left for at least (LEAP_TIME * 10 ms) and until sensor 4, 5 or 6 finds the line
		{
			if (speed_control_enable_flag == 1)
			{	
				controlSpeed();
				speed_control_enable_flag = 0;
				++leap_counter;
			}
		}
		orientation = (orientation + 270) % 360;
}

void goStraight() /*used for going for going straight in a crossroad*/
{		
		ref_l = DEFAULT_SPEED_REF; ref_r = DEFAULT_SPEED_REF;
		uint8_t leap_counter = 0;
		while (leap_counter < LEAP_TIME_STRAIGHT) //for (LEAP_TIME * 0.01) seconds go straight
		{
			if (speed_control_enable_flag == 1)
			{	
				controlSpeed();
				speed_control_enable_flag = 0;
				++leap_counter;
			}
		}
}
