
#include "../includes/global_variables.h"
#include "../includes/line_follow.h"
#include "../includes/maze.h"
#include "../includes/array.h"
#include "../includes/motor_control.h"
#include "../includes/defines.h"

void turnLedsOn()
{
	HAL_GPIO_WritePin(IR_LEDS_PORT, IR_LEDS_PIN, GPIO_PIN_SET);
}

void turnLedsOff()
{
	HAL_GPIO_WritePin(IR_LEDS_PORT, IR_LEDS_PIN, GPIO_PIN_RESET);
}

void turnAround()
{
	if (mode == EXPLORATION_MODE)
		insertElement(&directions, 'B');
	
	if (prev_left_motor_direction == FORWARD)
	{
  		ref_l = 0; ref_r = 0; //stop motors, wait, change left motor's direction, then turn around
		  controlSpeed();
			HAL_Delay(100);
			setDirection(BACKWARD, FORWARD);
			prev_left_motor_direction = BACKWARD;
	}
	ref_l = 300; ref_r = 300;
	
	while ((SENSOR_4 < THRESHOLD) && (SENSOR_5 < THRESHOLD) && (SENSOR_6 < THRESHOLD)) 
	//while sensor 4, 5 or 6 do not find the line
	{
		if (speed_control_enable_flag == 1)
			{	
				controlSpeed();
				speed_control_enable_flag = 0;
			}
	}
	ref_l = 0; ref_r = 0; //stop motors after turning around and wait
	controlSpeed();
	HAL_Delay(100);
	orientation = (orientation + 180) % 360;
}

uint8_t getSensorCode() // get a code in a uint8 variable from all sensors, each bit corresponds to one sensor
{
	uint8_t code = 0;
	/* senzor 1 */
	if (SENSOR_1 > THRESHOLD_R)
		code |= 1u;
	/* senzori 2 - 7 */
	for (int i = 0; i <= 5; ++i){ //for sensors 2 to 7
		if (adc_buf[i] > THRESHOLD)
			code |= 1u << (i + 1);
	}
	/* senzor 8 */
	if (SENSOR_8 > THRESHOLD)
		code |= 1u << 7;
	return code;
}

void followLine() //line follow function
{
	if (SENSOR_7 > THRESHOLD && SENSOR_4 <= THRESHOLD)
	{ //sensor 7
		 ref_l = DEFAULT_SPEED_REF - 400; ref_r = DEFAULT_SPEED_REF + 500;
	}
	else if (SENSOR_2 > THRESHOLD && SENSOR_5 <= THRESHOLD)
	{ //sensor 2
		ref_l = DEFAULT_SPEED_REF + 500; ref_r = DEFAULT_SPEED_REF - 400;
	}
	else if (SENSOR_6 > THRESHOLD)
	{ //sensor 6
	  ref_l = DEFAULT_SPEED_REF - 200; ref_r = DEFAULT_SPEED_REF + 200; //-100
	}
	else if (SENSOR_3 > THRESHOLD)
	{ //sensor 3
		ref_l = DEFAULT_SPEED_REF + 200; ref_r = DEFAULT_SPEED_REF - 200; //-100
	}
	else if ((SENSOR_4 > THRESHOLD) && (SENSOR_5 > THRESHOLD))
	{ // sensor 5 and 4
		ref_l = DEFAULT_SPEED_REF; ref_r = DEFAULT_SPEED_REF;
	}
	else if (SENSOR_4 > THRESHOLD)
	{ // sensor 4
		ref_l = DEFAULT_SPEED_REF + 100; ref_r = DEFAULT_SPEED_REF;
	}
	else if (SENSOR_5 > THRESHOLD)
	{ // sensor 5
	  ref_l = DEFAULT_SPEED_REF; ref_r = DEFAULT_SPEED_REF + 100;
	}
}
