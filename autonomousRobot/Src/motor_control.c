
#include "../includes/global_variables.h"
#include "../includes/motor_control.h"

const float a_r = 1.03710;
const float b_r = -0.95732; 
const float a_l = 1.01165; 
const float b_l = -0.93383;   // constants calculated using Tustin Method for discrete motors' control

int32_t c_rk = 0, c_rk1 = 0; // e(k), e(k-1), c(k), c(k-1) error and command signals for right motor
int32_t c_lk = 0, c_lk1 = 0; // e(k), e(k-1), c(k), c(k-1) error and command signals for left motor
int32_t e_lk = 0, e_lk1 = 0, e_rk = 0, e_rk1 = 0;

void setDirection(uint8_t leftDir, uint8_t rightDir){
	if (leftDir == BACKWARD)
	{ //change left wheel direciton to backward
		HAL_GPIO_WritePin(DIRECTION_PORT_LEFT_MOTOR, LEFT_MOTOR_DIRECTION_PIN_1, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(DIRECTION_PORT_LEFT_MOTOR, LEFT_MOTOR_DIRECTION_PIN_2, GPIO_PIN_SET);
	}
	else 
	{ //change left wheel direciton to backward
		HAL_GPIO_WritePin(DIRECTION_PORT_LEFT_MOTOR, LEFT_MOTOR_DIRECTION_PIN_1, GPIO_PIN_SET);
		HAL_GPIO_WritePin(DIRECTION_PORT_LEFT_MOTOR, LEFT_MOTOR_DIRECTION_PIN_2, GPIO_PIN_RESET);
	}
	/* same functions for the right wheel */
	if (rightDir == BACKWARD)
	{
		HAL_GPIO_WritePin(DIRECTION_PORT_RIGHT_MOTOR, RIGHT_MOTOR_DIRECTION_PIN_1, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(DIRECTION_PORT_RIGHT_MOTOR, RIGHT_MOTOR_DIRECTION_PIN_2, GPIO_PIN_SET);
	}
	else 
	{
		HAL_GPIO_WritePin(DIRECTION_PORT_RIGHT_MOTOR, RIGHT_MOTOR_DIRECTION_PIN_1, GPIO_PIN_SET);
		HAL_GPIO_WritePin(DIRECTION_PORT_RIGHT_MOTOR, RIGHT_MOTOR_DIRECTION_PIN_2, GPIO_PIN_RESET);
	}
}

void setPwmDutyCycle(uint16_t duty_cycle, uint8_t motor)
{
	// 0 = 0% duty cycle, 999 = 100% duty cycle for the given motor
	
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = duty_cycle;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	
	if (motor == LEFT_MOTOR)
	{
		HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1);
		HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
	}
	
	else if (motor == RIGHT_MOTOR)
	{
		HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3);
		HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_3);
	}
}

void controlSpeed(){ //speed control function, called at every 10 ms using speed_control_enable_flag
		if (ref_r == 0) 
			c_rk = 0;
		else
		{
			e_rk = ref_r - measured_speed_R; //calculate error and command signals
			c_rk = c_rk1 + a_r * e_rk + b_r * e_rk1;
			if (c_rk < 0)
				c_rk = 0;
			else if (c_rk > 999)
				c_rk = 999;
		}
		setPwmDutyCycle(c_rk, RIGHT_MOTOR); // set duty cycle; right motor
					
		if (ref_l == 0) 
			c_lk = 0;
		else
		{
			e_lk = ref_l - measured_speed_L; //calculate error and command signals
			c_lk = c_lk1 + a_l * e_lk + b_l * e_lk1;
			if (c_lk < 0)
				c_lk = 0;
			else if (c_lk > 999)
				c_lk = 999;
		}
		setPwmDutyCycle(c_lk, LEFT_MOTOR); // set duty cycle; left motor
		
		e_rk1 = e_rk; // update signals
		c_rk1 = c_rk;
		e_lk1 = e_lk;
		c_lk1 = c_lk;
		
}
