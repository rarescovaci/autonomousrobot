
#include "stm32f3xx_hal.h"

#ifndef CROSSROADS_H
#define CROSSROADS_H

void turnLeft(void);
void turnLeft2(void);
void turnRight(void);
void turnRight2(void);
void goStraight(void);

#endif
