
#include "stm32f3xx_hal.h"

#ifndef MAZE_H
#define MAZE_H

void solveMaze(void);
void checkForCrossroad(void);
void manageCrossroad(void);

#endif
