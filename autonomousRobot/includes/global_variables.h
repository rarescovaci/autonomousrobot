
#ifndef GLOBAL_VARIABLES_H
#define GLOBAL_VARIABLES_H

#include "stm32f3xx_hal.h"
#include "defines.h"
#include "array.h"

extern TIM_HandleTypeDef htim1; //timer handlers
extern TIM_OC_InitTypeDef sConfigOC;
extern UART_HandleTypeDef huart4;

extern volatile uint32_t measured_speed_R; //measured motor speeds
extern volatile uint32_t measured_speed_L;

extern volatile uint16_t input_capture[4]; //input capture array

extern uint32_t ref_r; // reference speed for right motor (pulses/second)
extern uint32_t ref_l; // reference speed for left motor

extern uint8_t crossroad; // crossroad code
extern uint8_t mode; //mode
extern uint8_t maze_solved; // used to check if maze is solved
extern uint8_t maze_solved_counter;
extern const uint8_t TIMEOUT;

extern volatile uint8_t adc_buf[6]; // sensor array 2-7
extern volatile uint8_t adc2_buf[2]; // sensor array 1,8
extern uint8_t sensor_code;
extern uint8_t prev_left_motor_direction;

extern Array directions;
extern uint8_t crossroad_number;

extern const uint8_t THRESHOLD; // if adc value > threshold -> the line is detected
extern const uint8_t THRESHOLD_R; // separate threshold value for rightmost sensor because it has a higher voltage output

extern const uint32_t DEFAULT_SPEED_REF; // default reference for motors speed

extern uint8_t turnaround_counter; // used to check if turning around is necessary
extern const uint8_t TURNAROUND_ENABLE;

extern uint8_t speed_control_enable_flag;

extern uint16_t orientation;
	
#endif
