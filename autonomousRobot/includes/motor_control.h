
#include "stm32f3xx_hal.h"

#ifndef MOTORCONTROL_H
#define MOTORCONTROL_H

void setPwmDutyCycle(uint16_t duty, uint8_t motor);
void controlSpeed(void);
void setDirection(uint8_t leftDir, uint8_t rightDir);

#endif
