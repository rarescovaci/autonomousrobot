
#include "stm32f3xx_hal.h"

#ifndef LINEFOLLOW_H
#define LINEFOLLOW_H

void turnLedsOn(void);
void turnLedsOff(void);
uint8_t getSensorCode(void); 
void followLine(void);
void turnAround(void);

#endif
