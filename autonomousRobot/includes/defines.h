
#ifndef DEFINES_H /* define guards */
#define DEFINES_H

#define DIRECTION_PORT_LEFT_MOTOR GPIOC
#define DIRECTION_PORT_RIGHT_MOTOR GPIOA
#define LEFT_MOTOR_DIRECTION_PIN_1 GPIO_PIN_9
#define LEFT_MOTOR_DIRECTION_PIN_2 GPIO_PIN_8
#define RIGHT_MOTOR_DIRECTION_PIN_1 GPIO_PIN_8
#define RIGHT_MOTOR_DIRECTION_PIN_2 GPIO_PIN_9

#define LEFT_MOTOR 1
#define RIGHT_MOTOR 2

#define BACKWARD 0
#define FORWARD 1

#define EXPLORATION_MODE 0
#define OPTIMIZED_MODE 1

#define IR_LEDS_PORT GPIOB
#define IR_LEDS_PIN GPIO_PIN_7

#define SENSOR_1 adc2_buf[1]
#define SENSOR_2 adc_buf[0]
#define SENSOR_3 adc_buf[1]
#define SENSOR_4 adc_buf[2]
#define SENSOR_5 adc_buf[3]
#define SENSOR_6 adc_buf[4]
#define SENSOR_7 adc_buf[5]
#define SENSOR_8 adc2_buf[0]

#endif
