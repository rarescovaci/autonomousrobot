
#include "stm32f3xx_hal.h"

#ifndef ARRAY_H
	#define ARRAY_H
	
	typedef struct {
		uint8_t *array;
		uint8_t used;
		uint8_t size;
		
} Array;


void initArray(Array *a, uint8_t initialSize);

void insertElement(Array *a, uint8_t element);

void removeElement(Array *a, uint8_t size, uint8_t index);

void freeArray(Array *a);

void shiftArray(Array *a, uint8_t index);

void processArray(Array *a);

#endif
